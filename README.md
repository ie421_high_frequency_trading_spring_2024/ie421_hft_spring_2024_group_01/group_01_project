# Raspberry Pi 5 and Compute Module 4 GPS Grandmaster Development


This README offers a comprehensive overview of the project, encompassing setup guidelines, advanced performance tuning methods, and the repository's structural organization. 

[[_TOC_]]

## Team 

![alt text](imgs/laxmiv2_prof.jpg "Laxmi 'Lux' Vijayan"){width=100 height=140px}  
***Laxmi Vijayan**, Team Lead*

As a Master’s student in Computer Science, I've thrived in fields like User Interface Design, Internet of Things, AI, and High-Frequency Trading Technology. My background in biomedical sciences, combined with my technical proficiency in programming languages (Python, Java, Bash, Ruby, C++, SQL) and data science, and my hands-on experience with microcontrollers, has enabled me to craft innovative solutions across different sectors, including HFT. I'm deeply passionate about designing intuitive user experiences and addressing real-world challenges. I am eager to apply my interdisciplinary knowledge and technical expertise to develop cutting-edge trading solutions and drive advancements in the financial industry.

Email: [laxmiv2@illinois.edu](mailto:laxmiv2@illinois.edu)  
LinkedIn: https://www.linkedin.com/in/luxvijayan/

![alt text](imgs/IMG_0923.PNG "Yiqing"){width=130 height=130px}  
***Yiqing Huang**, Data Analyst* 

As a Master's student in Mathematics with a strong foundation in Computer Science, I bring a unique blend of skills to the high-frequency trading domain, specializing in data analysis. My technical proficiency spans a variety of programming languages including Python, C++, and SQL, and is complemented by in-depth knowledge in data science, cloud computing, and machine learning. Passionate about leveraging innovative solutions to tackle real-world challenges, I am dedicated to utilizing my interdisciplinary knowledge and analytical expertise to drive advancements in the financial industry, particularly in developing cutting-edge trading solutions.

Email: [yh50@illinois.edu](mailto:yh50@illinois.edu)  
LinkedIn:  www.linkedin.com/in/yiqing5678-huang

![](imgs/hy19_pfp.jpg){width=100}<br>
***Haoyuan You**, Hardware Specialist*

Hi! My name is Haoyuan You, currently pursuing my Master's degree in Electrical and Electronics Engineering at UIUC. My expertise lies in robotics and embedded systems. Over the past four years, I've honed my skills in STM32 microcontroller and hardware interface development through extracurricular organizations. Concurrently, I've accumulated an equal span of experience in ROS development during my lab engagements. Also as an active open-source contributor, my projects have positively impacted thousands of users, reflecting my commitment to advancing technology for the greater community.


Email: [hy19@illinois.edu](mailto:hy19@illinois.edu)  
LinkedIn: https://www.linkedin.com/in/haoyuan-you-2a0341193/

![alt text](imgs/Victor.jpeg "Victor"){width=130 height=130px}  
***Victor Li**, Network Specialist*

As a junior majoring in Computer Science and Philosophy, my academic and practical journey has been deeply rooted in the exploration of machine learning, networking, and microservices. This foundation has not only sharpened my technical acumen but also fueled my passion for innovative problem-solving within complex systems. My experiences have allowed me to delve into the intricacies of data transmission and the efficiency of microservices architecture, fostering a keen interest in building scalable, robust solutions. In parallel, my fascination with the financial industry, especially proprietary trading and hedge funds, has grown. The allure of leveraging technical expertise to influence trading strategies and financial outcomes drives my ambition. I am captivated by the dynamic, fast-paced nature of the trading world, which resonates with my intrinsic motivation for constant learning and tackling challenging problems.

Email: [wentaol4@illinois.edu](mailto:wentaol4@illinois.edu)   
LinkedIn: https://www.linkedin.com/in/isvictorli/   

— Advised by Professor David Lariviere

## Project Summary

**Objective**  
The primary goal of this project is to investigate the feasibility and efficiency of using Raspberry Pi 5 or Compute Module 4, equipped with a u-blox GPS receiver, as a GPS Grandmaster for precise time synchronization. By leveraging optimization techniques from high-frequency trading (HFT), this study aims to enhance the accuracy and reliability of time synchronization, a critical component in financial trading and other time-sensitive applications.

**Background**  
In the realm of high-frequency trading, nanoseconds can represent significant financial gains or losses. Precise time synchronization across trading systems is crucial to maintain fairness and efficiency in the market. Traditional GPS Grandmaster solutions are often expensive and complex. This project explores a cost-effective, scalable alternative using widely available hardware, combined with advanced software optimizations, to meet or exceed current standards in time accuracy.

**Impact and Relevance**  
By achieving high levels of time synchronization precision, our solution has the potential to democratize access to technology essential for HFT and other sectors requiring synchronized time-stamping, such as telecommunications, distributed networks, and scientific research. The implications of this research extend beyond trading, offering insights and methodologies applicable to a wide range of industries.

**Approach**  
Our methodology involves:

1. Setting up Raspberry Pi units with u-blox GPS receivers.
2. Implementing and testing various software and hardware optimization techniques.
3. Comparing performance between baseline Raspberry Pi 5 and Compute Module 4, as well as optimized versions. 

Through systematic experimentation and analysis, we aim to identify the most effective combinations of hardware and software settings to achieve optimal time synchronization performance.

**Innovation**  
This project stands at the intersection of hardware engineering and financial technology, embodying a multidisciplinary approach to solving a real-world problem. By applying HFT optimization techniques to Raspberry Pi-based systems, we are pushing the boundaries of what is possible with consumer-grade electronics in professional and critical infrastructures.

**Future Work**  
The findings from this project will lay the groundwork for future research and development in affordable, accurate timekeeping technologies. We would like to compare the performance of GPS Grandmaster against traditional GPS Grandmaster Clocks under similar conditions. 

## Repository Organization

- `/src`: Source code directory containing scripts for all optimizations.
- `/docs`: Background research, documentation, including detailed setup guides, tuning techniques, data sheets, and project reports.
- `/tools`: Utility scripts for dependencies installation and configuration setup.
- `/tests`: Scripts for testing the performance of the time synchronization, including pre- and post-optimization results. 
- `/results`: Stores the output from any statistical analyses.
- `/data`: Raw data from test results. 
- `/imgs`:  Images used in any documentation, including README.md and project reports. 
- `README.md`: An overview of the project, setup instructions, and other essential information.

## Setups

### Local Systems
![](imgs/Local_Set_Up.jpg)

### Lab Systems
![](imgs/Lab_Set_Up.jpg)

## Final Report

For more details, please review our [final_report.md](docs/final_report.md)
